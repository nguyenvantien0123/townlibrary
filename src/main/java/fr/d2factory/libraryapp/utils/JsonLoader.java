package fr.d2factory.libraryapp.utils;

import com.google.gson.Gson;
import fr.d2factory.libraryapp.book.Book;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class JsonLoader<T> {
    /**
     * 
     * @param <T>
     * @param reader a json file reader
     * @param objectType type of output object
     * @return a list of objects 
     */
    public static <T> List<T> jsonToList(Reader reader, Class<T[]> objectType) {
        return Arrays.asList(new Gson().fromJson(reader, objectType));
    }

    /**
     * 
     * @param file a book file
     * @return a list of book objects
     */
    public Optional<List<Book>> loadBook(String file) {
        List<Book> result = null;
        try (Reader reader = new FileReader(file)) {
            //Convert JSON File to Java Object
            result = jsonToList(reader, Book[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(result);
    }

}
