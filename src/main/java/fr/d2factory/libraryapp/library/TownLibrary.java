package fr.d2factory.libraryapp.library;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import fr.d2factory.libraryapp.book.Book;
import fr.d2factory.libraryapp.book.BookNotFoundException;
import fr.d2factory.libraryapp.book.BookRepository;
import fr.d2factory.libraryapp.borrowing.BorrowingRepository;
import fr.d2factory.libraryapp.member.Member;
import fr.d2factory.libraryapp.member.MemberRepository;
import lombok.*;
import lombok.Getter;

/**
 *
 * This class control all library activities
 */
@Getter
@Setter
public class TownLibrary implements Library {
	//The repositories
    private BookRepository bookRepository;
    private MemberRepository memberRepository;
    private BorrowingRepository borrowingRepository;
    
    //Unique install of the class
    private static TownLibrary instance;

    /**
     * Ensure that user can't create object from contructor
     */
    private TownLibrary() {

    }

    /**
     *
     * @return unique instance of this class
     */
    public static TownLibrary getInstance() {
        if (instance == null) {
            synchronized (TownLibrary.class) {
                if (instance == null) {
                    instance = new TownLibrary();
                }
            }
        }
        return instance;
    }
    
    public TownLibrary bookRepository(BookRepository bookRepository) {
    	this.bookRepository = bookRepository;
    	return this;
    }
    
    public TownLibrary memberRepository(MemberRepository memberRepository) {
    	this.memberRepository = memberRepository;
    	return this;
    }
    
    public TownLibrary borrowingRepository(BorrowingRepository borrowingRepository) {
    	this.borrowingRepository = borrowingRepository;
    	return this;
    }

    /**
     * Member borrows a book at a time
     *
     * @param isbnCode isbd code of a book
     * @param member member who wants borrow the book
     * @param borrowedAt the time when member borrows the book
     * @return the book object having isbnCode
     * @throws HasLateBooksException if member as
     * @throws BookNotFoundException
     */
    @Override
    public Optional<Book> borrowBook(long isbnCode, Member member, LocalDate borrowedAt) throws HasLateBooksException, BookNotFoundException {
        /*
         * check if member has books to return. Note that if a member is late with their books they cannot borrow any new books before
         * returning the previous ones.
         */
        if (this.borrowingRepository.hasLateMember(member)) {
            throw new HasLateBooksException("Member has books which must be returned");
        }

        //find the book by isbn code
        Book book = this.bookRepository.findBook(isbnCode)
                .orElseThrow(()
                        -> new BookNotFoundException("Book not found exception :" + isbnCode)
                );

        if (checkAvailable(book)) {
            //save borrowed book
            this.borrowingRepository.saveBookBorrow(book, member, borrowedAt);
            return Optional.ofNullable(book);
        } else {
            return Optional.empty();
        }

    }

    @Override
    public void returnBook(Book book, Member member) {
        //remove book from borrowed books
        this.borrowingRepository.returnBook(book, member);
        //save member (money)
        this.memberRepository.updateMember(member);
    }

    /**
     *
     * @param book
     * @return true if the book is available to borrow
     */
    @Override
    public boolean checkAvailable(Book book) {
        return !this.borrowingRepository.contains(book);
    }


    @Override
    public Optional<List<Book>> getLateBooks(Member member) {
    	//get the books that the member must return before can borrow other books
        return this.borrowingRepository.getLateBooks(member);
    }
    
    public void clear(){
        this.bookRepository.clear();
        this.memberRepository.clear();
        this.borrowingRepository.clear();
    }

}
