package fr.d2factory.libraryapp.book;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import fr.d2factory.libraryapp.repository.Repository;
;

/**
 * The book repository emulates a book database via 1 HashMaps
 */
public class BookRepository extends Repository{

    /**
     * all books in the library, include available books and borrowed books
     */
    private Map<ISBN, Book> allBooks = new HashMap<>();
    
    //Unique install of the class
    private static BookRepository instance;

    /**
     * Ensure that user can't create object from contructor
     */
    private BookRepository() {

    }

    /**
     *
     * @return unique instance of this class
     */
    public static BookRepository getInstance() {
        if (instance == null) {
            synchronized (BookRepository.class) {
                if (instance == null) {
                    instance = new BookRepository();
                }
            }
        }
        return instance;
    }

    public void addBooks(List<Book> books) {
        books.forEach(book -> allBooks.put(book.getIsbn(), book));
    }

    /**
     * Search a book by isbn code
     *
     * @param isbnCode
     * @return a book having isbn code if found
     */
    public Optional<Book> findBook(long isbnCode) {
        ISBN isbn = new ISBN.ISBNBuilder()
                .isbnCode(isbnCode)
                .build();
        return Optional.ofNullable(this.allBooks.get(isbn));
    }

    public List<Book> getAllBooks() {
        return new ArrayList<>(allBooks.values());
    }

    /**
     * reset the repository
     */
    @Override
    public void clear() {
        this.allBooks.clear();
    }
}
