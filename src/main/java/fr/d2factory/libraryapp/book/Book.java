package fr.d2factory.libraryapp.book;

import lombok.*;

/**
 * A simple representation of a book
 */
@Getter
@Builder
@EqualsAndHashCode
public class Book {
    String title;
    String author;
    ISBN isbn;
}
