package fr.d2factory.libraryapp.borrowing;

import fr.d2factory.libraryapp.book.BookNotFoundException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.d2factory.libraryapp.book.Book;
import fr.d2factory.libraryapp.book.BookRepository;
import fr.d2factory.libraryapp.book.ISBN;
import fr.d2factory.libraryapp.member.Member;
import fr.d2factory.libraryapp.repository.Repository;

/**
 * The book repository emulates a borrowed books
 */
public class BorrowingRepository extends Repository{
    /**
     * all borrowed books of the library
     */
    private Map<ISBN, BorrowingItem> borrowedBooks = new HashMap<>();

    //Unique instance of the class
    private static BorrowingRepository instance;

    /**
     * Ensure that user can't create object from contructor
     */
    private BorrowingRepository() {

    }

    /**
     *
     * @return unique instance of this class
     */
    public static BorrowingRepository getInstance() {
        if (instance == null) {
            synchronized (BookRepository.class) {
                if (instance == null) {
                    instance = new BorrowingRepository();
                }
            }
        }
        return instance;
    }
    

    /**
     * Save the book borrowed by member at the time
     *
     * @param book
     * @param member
     * @param time
     */
    public void saveBookBorrow(Book book, Member member, LocalDate time) {
        this.borrowedBooks.put(book.getIsbn(), new BorrowingItem.BorrowingItemBuilder()
                .book(book)
                .member(member)
                .borrowedAt(time)
                .build());
    }

    /**
     *
     * @param book
     * @return the date at which the book is borrowed
     * @throws BookNotFoundException
     */
    public Optional<LocalDate> findBorrowedBookDate(Book book) throws BookNotFoundException {
        BorrowingItem borrowedItem = Optional.ofNullable(this.borrowedBooks.get(book.getIsbn()))
                                             .orElseThrow(() -> new BookNotFoundException("Book not borrowed exception"));
        return Optional.ofNullable(borrowedItem.getBorrowedAt());
    }

    /**
     *
     * @param member
     * @return true if member has any borrowed book which is late
     */
    public boolean hasLateMember(Member member) {
        return borrowedBooks.values()
                .stream()
                .anyMatch(item -> item.isBorrowedBy(member)
                && item.isLate()
                );
    }

    /**
     *
     * @param member
     * @param book
     * @return true if the member borrowed the book
     */
    public boolean contains(Member member, Book book) {
        return borrowedBooks.values()
                .stream()
                .anyMatch(item -> item.isBook(book)
                && item.isBorrowedBy(member)
                );
    }

    /**
     *
     * @param book
     * @return true if the book is already borrowed
     */
    public boolean contains(Book book) {
        return borrowedBooks.containsKey(book.getIsbn());
    }

    /**
     *
     * @param member
     * @return true if there is any book borrowed by the member
     */
    public boolean contains(Member member) {
        return borrowedBooks.values()
                .stream()
                .anyMatch(item -> item.isBorrowedBy(member)
                );
    }

    /**
     * Remove the book from the borrowed books
     *
     * @param book a returned book
     */
    public void returnBook(Book book, Member member) {
        BorrowingItem item = this.borrowedBooks.get(book.getIsbn());
        member.payBook(item.borrowedDays());
        this.borrowedBooks.remove(book.getIsbn());
    }

    /**
     *
     * @param member
     * @return the list of books member must return before borrow others
     */
    public Optional<List<Book>> getLateBooks(Member member) {
        return Optional.ofNullable(this.borrowedBooks.values()
                .stream()
                .filter(item -> item.isBorrowedBy(member) && item.isLate())
                .map(item -> item.getBook())
                .collect(Collectors.toList())
        );

    }
    
    /**
     * Reset the repository
     */
    @Override
    public void clear(){
        this.borrowedBooks.clear();
    }
}
