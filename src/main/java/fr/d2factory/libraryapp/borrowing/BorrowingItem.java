package fr.d2factory.libraryapp.borrowing;

import java.time.LocalDate;

import fr.d2factory.libraryapp.book.Book;
import fr.d2factory.libraryapp.member.Member;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import static java.time.temporal.ChronoUnit.DAYS;

/**
 *
 * Simple representation of borrowing item describing who borrow which book and
 * when borrow it
 *
 */
@Getter
@Builder
@EqualsAndHashCode
public class BorrowingItem {
    private Book book;
    private Member member;
    private LocalDate borrowedAt;

    /**
     *
     * @return true if the member is late
     */
    public boolean isLate() {
        return borrowedDays() > member.maxAllowedDays();
    }

    /**
     *
     * @param m a member to verify
     * @return true if the book is borrowed by member m
     */
    public boolean isBorrowedBy(Member m) {
        return this.member.equals(m);
    }

    /**
     * 
     * @param book
     * @return true if the record is related to the book
     */
    public boolean isBook(Book book) {
        return this.book.equals(book);
    }
    
    /**
     * 
     * @return number of days from borrowed date to now
     */
    public int borrowedDays(){
    	return (int) DAYS.between(borrowedAt, LocalDate.now());
    }
}
