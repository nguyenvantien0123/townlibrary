package fr.d2factory.libraryapp.member;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.d2factory.libraryapp.repository.Repository;

/**
 * The simple member repository emulates a member database
 */
public class MemberRepository extends Repository {

    //All members of the library
    private Map<String, Member> members = new HashMap<>();

    //Unique instance of this class
    private static MemberRepository instance;

    /**
     * Ensure that user can't create object from contructor
     */
    private MemberRepository() {

    }

    /**
     *
     * @return unique instance of this class
     */
    public static MemberRepository getInstance() {
        if (instance == null) {
            synchronized (MemberRepository.class) {
                if (instance == null) {
                    instance = new MemberRepository();
                }
            }
        }
        return instance;
    }

    /**
     * Add new member
     * @param member 
     */
    public void addMember(Member member) {
        this.members.put(member.getId(), member);
    }

    public void addMembers(List<Member> members) {
        members.forEach(member -> addMember(member));
    }

    /**
     * Update the member information
     * @param member 
     */
    public void updateMember(Member member) {
        members.put(member.getId(), member);
    }

    public List<Member> getAllMembers() {
        return new ArrayList<>(members.values());
    }

    /**
     * reset the repository
     */
    @Override
    public void clear() {
        this.members.clear();
    }
}
