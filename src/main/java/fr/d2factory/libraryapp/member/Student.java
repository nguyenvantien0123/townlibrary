package fr.d2factory.libraryapp.member;

import java.time.LocalDate;
import static java.time.temporal.ChronoUnit.DAYS;
import lombok.*;

/**
 * This class represenst Student member
 */
@Getter
@Setter
@NoArgsConstructor
public class Student extends Member {
    /**
     * Define or load the constants related toe the student members
     */
    private final float NORMAL_PRICE = 0.10f;
    private final float PENATY_PRICE = 0.15f;
    private final int FREE_DAYS_FIRST_YEAR = 15;
    private final int MAX_DAYS = 30;
    private final int DAYS_OF_YEAR = 365;
    
    /**
     * The date that student start his studing at the university
     */

    private LocalDate beginUniversityDate;

    public Student(String id, float wallet) {
        super(id, wallet);
    }

    @Override
    public int freeDayNumber() {
        if (isFirstYearStudent()) {
            return FREE_DAYS_FIRST_YEAR;
        }
        return 0;
    }
    
    public boolean isFirstYearStudent(){
        return DAYS.between(beginUniversityDate, LocalDate.now()) < DAYS_OF_YEAR;
    }

    @Override
    public float normalPrice() {
        return NORMAL_PRICE;
    }

    @Override
    public float penaltyPrice() {
        return PENATY_PRICE;
    }

    @Override
    public int maxAllowedDays() {
        return MAX_DAYS;
    }

}
