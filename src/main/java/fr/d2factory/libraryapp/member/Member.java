package fr.d2factory.libraryapp.member;

import fr.d2factory.libraryapp.library.Library;
import lombok.*;

/**
 * A member is a person who can borrow and return books to a {@link Library} A
 * member can be either a student or a resident
 */
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public abstract class Member {
    //There are only 2 type of member : student and resident
    public enum MemberType {
        STUDENT,
        RESIDENT;
    }

    private String id;

    /**
     * An initial sum of money the member has
     */
    @EqualsAndHashCode.Exclude
    private float wallet;

    /**
     *
     * @return The normal price
     */
    public abstract float normalPrice();

    /**
     *
     * @return The price if the book is returned lately
     */
    public abstract float penaltyPrice();

    /**
     * Number of free day. It depends on the member type. Generally, I use here
     * a method to calculate it instead of attribute because for the student,
     * this value change in the time
     *
     * @return number of free day, for example 15 in the case of first year
     * student
     */
    public abstract int freeDayNumber();

    /**
     * The number of days maximum that the member can borrow a book.
     */
    public abstract int maxAllowedDays();

    /**
     * The member should pay their books when they are returned to the library.
     * I use here template design pattern which allow me to define the general
     * calculation for all member types.
     *
     * @param numberOfDays the number of days they kept the book
     */
    public final void payBook(int numberOfDays) {
        float price = 0;
        if (numberOfDays > maxAllowedDays()) {
            price = normalPrice() * (maxAllowedDays() - this.freeDayNumber()) // Normal price party
                    + penaltyPrice() * (numberOfDays - maxAllowedDays());// and penalty price party
        } else if (numberOfDays > this.freeDayNumber()) {
            price = this.normalPrice() * (numberOfDays - this.freeDayNumber()); // Only normal price party
        };
        this.wallet -= price;
    }

}
