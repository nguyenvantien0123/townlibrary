package fr.d2factory.libraryapp.member;

import lombok.*;

/**
 * This class represenst Student member
 */
@NoArgsConstructor
public class Resident extends Member {

    /**
     * Define or load the constants related toe the student members
     */
    public final static float NORMAL_PRICE = 0.10f;
    public final static float PENALTY_PRICE = 0.20f;
    public final static int FREE_DAYS = 0;
    public final static int MAX_DAYS = 60;

    public Resident(String id, float wallet) {
        super(id, wallet);
    }

    @Override
    public int freeDayNumber() {
        return FREE_DAYS;
    }

    @Override
    public float normalPrice() {
        return NORMAL_PRICE;
    }

    @Override
    public float penaltyPrice() {
        return PENALTY_PRICE;
    }

    @Override
    public int maxAllowedDays() {
        return MAX_DAYS;
    }
}
