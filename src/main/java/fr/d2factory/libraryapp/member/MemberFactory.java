package fr.d2factory.libraryapp.member;

import fr.d2factory.libraryapp.member.Member.MemberType;
import java.time.LocalDate;
import java.util.EnumMap;
import java.util.function.Supplier;

/**
 * The factory of {@link Member}.
 */
public final class MemberFactory {

    private MemberFactory() {

    }

    /**
     * Member constructors
     */
    private static final EnumMap<MemberType, Supplier<Member>> members = new EnumMap<>(MemberType.class);

    static {
        members.put(MemberType.STUDENT, Student::new);
        members.put(MemberType.RESIDENT, Resident::new);
    }

    /**
     *
     * @param memberType
     * @return a member object
     */
    public static Member create(MemberType memberType) {
        return members.get(memberType).get();
    }

    /**
     * Create a student member with detail info
     * @param id
     * @param money
     * @param startUniversityDate
     * @return
     */
    public static Student createStudent(String id, float money, LocalDate startUniversityDate) {
        Student student = (Student) create(MemberType.STUDENT);
        student.setId(id);
        student.setWallet(money);
        student.setBeginUniversityDate(startUniversityDate);
        return student;
    }
    
    /**
     * Create a resident member
     * @param id
     * @param money
     * @return
     */
    public static Resident createResident(String id, float money){
        Resident resident = (Resident) create(MemberType.RESIDENT);
        resident.setId(id);
        resident.setWallet(money);
        return resident;
    }
}
