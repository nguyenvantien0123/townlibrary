package fr.d2factory.libraryapp.library;

import fr.d2factory.libraryapp.book.Book;
import fr.d2factory.libraryapp.book.BookNotFoundException;
import fr.d2factory.libraryapp.book.BookRepository;
import fr.d2factory.libraryapp.borrowing.BorrowingRepository;
import fr.d2factory.libraryapp.member.Member;
import fr.d2factory.libraryapp.member.MemberFactory;
import fr.d2factory.libraryapp.member.MemberRepository;
import fr.d2factory.libraryapp.member.Resident;
import fr.d2factory.libraryapp.utils.JsonLoader;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import java.io.File;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import lombok.extern.log4j.Log4j;
import org.junit.After;

@Log4j
public class LibraryTest {

    private Library library;
    private BookRepository bookRepository;
    private MemberRepository memberRepository;
    private BorrowingRepository borrowingRepository;

    long isbnCode;
    private Member resident;
    private Member firstYearStudent;
    private Member otherStudent;
    private final float  DELTA = 0.00001f;    

    /**
     * Prepare data before each test
     */
    @Before
    public void setup() {
        Optional<List<Book>> books = new JsonLoader<Book>().loadBook(new StringBuilder().append("src")
        																				.append(File.separatorChar)
        																				.append("test")
        																				.append(File.separatorChar)
        																				.append("resources")
        																				.append(File.separatorChar)
        																				.append("books.json")
        																				.toString()
        															 );
        //create an empty book repository
        this.bookRepository = BookRepository.getInstance();
        //create an empty member repository
        this.memberRepository = MemberRepository.getInstance();
        //create an empty borrowing repository
        this.borrowingRepository = BorrowingRepository.getInstance();

        //Prepare common data for tests
        resident = MemberFactory.createResident("1", 100);
        firstYearStudent = MemberFactory.createStudent("2", 100, LocalDate.of(2018, Month.SEPTEMBER, 1));
        otherStudent = MemberFactory.createStudent("3", 100, LocalDate.of(2017, Month.SEPTEMBER, 1));

        //add some members
        this.memberRepository.addMember(resident);
        this.memberRepository.addMember(firstYearStudent);
        this.memberRepository.addMember(otherStudent);

        //add books to repository
        this.bookRepository.addBooks(books.orElse(new ArrayList<>()));
        this.library =  TownLibrary.getInstance()
                				   .bookRepository(bookRepository)
                				   .memberRepository(memberRepository)
                				   .borrowingRepository(borrowingRepository)
                				   ;

        isbnCode = this.bookRepository.getAllBooks().get(0).getIsbn().getIsbnCode();

    }
    
    
    /**
     * Clear all data after each test
     */
    @After
    public void clearAll(){
        this.bookRepository.clear();
        this.memberRepository.clear();
        this.borrowingRepository.clear();
    }
            

    @Test
    public void test1MemberCanBorrowABookIfBookIsAvailable() {
        log.info("Test 1 : member can borrow a book if it is available");
        //Try to borrow a book, and check if it is saved
        Optional<Book> book = Optional.empty();
        try {
            book = this.library.borrowBook(isbnCode, resident, LocalDate.now());
        } catch (HasLateBooksException ex) {
            log.warn("Member has books to return", ex);
        } catch (BookNotFoundException ex) {
            log.error("Book not found exception :" + isbnCode, ex);
        }
        assertTrue("Verify that the book exists", book.isPresent());
        assertTrue("Verify that the borrowing book by the member is well saved", borrowingRepository.contains(resident, book.get()));
    }

    @Test(expected = BookNotFoundException.class)
    public void test2InvalidIsbnWhenBorrow() throws HasLateBooksException, BookNotFoundException {
        log.info("Test 2 : Raise an exception when trying to borrowing a book with invalid isbn code");
        //prepare data for this test
        Member member = resident;
        long isbnCode = 1234L;
        //Try to borrow a book, and check if it is saved
        Optional<Book> book = Optional.empty();
        book = this.library.borrowBook(isbnCode, member, LocalDate.now());
    }

    @Test
    public void test3BorrowedBookIsNoLongerAvailable() {
        log.info("Test 3 : Borrowed book is no longer available");
        //prepare data for this test
        Member member = resident;
        long isbnCode = this.isbnCode;
        //Try to borrow a book
        Optional<Book> book = Optional.empty();
        try {
            book = this.library.borrowBook(isbnCode, member, LocalDate.now());
        } catch (HasLateBooksException ex) {
            log.warn("Member has books to return", ex);
        } catch (BookNotFoundException ex) {
            log.error("Book not found exception :" + isbnCode, ex);
        }
        assertFalse("Verify that the borrowed book is not available", this.library.checkAvailable(book.get()));
    }

    @Test
    public void test4ResidentsAreTaxed10CentsForEachDayTheyKeepABook() throws HasLateBooksException, BookNotFoundException {
    	log.info("Test 4 : residents are texted 10 cents per day");	        
        assertEquals(0.1f, resident.normalPrice(), DELTA);
        float moneyBefore = resident.getWallet();
        Book book = this.library.borrowBook(isbnCode, resident, LocalDate.now().minusDays(5)).get();
        this.library.returnBook(book, resident);
        float moneyAfter = resident.getWallet();
        assertEquals(0.5f, moneyBefore - moneyAfter, DELTA);
    }

    @Test
    public void test5StudentsPay10CentsTheFirst30Days() throws HasLateBooksException, BookNotFoundException {
    	log.info("Test 5 : students are texted 10 cents per day for first 30 days");
        assertEquals(0.1f, otherStudent.normalPrice(), DELTA);
        float moneyBefore = otherStudent.getWallet();
        Book book = this.library.borrowBook(isbnCode, otherStudent, LocalDate.now().minusDays(30)).get();
        this.library.returnBook(book, otherStudent);
        float moneyAfter = otherStudent.getWallet();
        assertEquals(0.1f*30, moneyBefore - moneyAfter, DELTA);
    }

    @Test
    public void test6StudentsInFirstYearAreNotTaxedForTheFirst15days() throws HasLateBooksException, BookNotFoundException {
        log.info("Test 6 : students in first year are not taxted for the first 15 days");
        float moneyBefore = firstYearStudent.getWallet();
        Book book = this.library.borrowBook(isbnCode, firstYearStudent, LocalDate.now().minusDays(15)).get();
        this.library.returnBook(book, firstYearStudent);
        float moneyAfter = firstYearStudent.getWallet();
        assertEquals(moneyBefore, moneyAfter, DELTA);
    }

    @Test
    public void test7StudentsPay15CentsForEachDayTheyKeepABookAfterTheInitial30Days() throws HasLateBooksException, BookNotFoundException {
        log.info("Test 7 : students pay 15 cents for each day after the initial 30 days");
        float moneyBefore = otherStudent.getWallet();
        Book book = this.library.borrowBook(isbnCode, otherStudent, LocalDate.now().minusDays(40)).get();
        this.library.returnBook(book, otherStudent);
        float moneyAfter = otherStudent.getWallet();
        assertEquals(0.1f*30 + 10*0.15, moneyBefore - moneyAfter, DELTA);
    }

    @Test
    public void test8ResidentsPay20CentsForEachDayTheyKeepABookAfterTheInitial60days() throws HasLateBooksException, BookNotFoundException {
        log.info("Test 8 : residents pay 20 cents for each day they keep a book after the initial 60 days");
        float moneyBefore = resident.getWallet();
        Book book = this.library.borrowBook(isbnCode, resident, LocalDate.now().minusDays(65)).get();
        this.library.returnBook(book, resident);
        float moneyAfter = resident.getWallet();
        assertEquals(0.1f*60 + 5*0.2, moneyBefore - moneyAfter, DELTA);
    }

    @Test(expected = HasLateBooksException.class)
    public void test9MembersCannotBorrowBookIfTheyHaveLateBooks() throws HasLateBooksException, BookNotFoundException {
        log.info("Test 9 : Member cannot borrow book if they have late books");
        Member member = resident;
        this.library.borrowBook(isbnCode, member, LocalDate.now().minusDays(Resident.MAX_DAYS + 15));
        long isbnCode2 = this.bookRepository.getAllBooks().get(2).getIsbn().getIsbnCode();
        this.library.borrowBook(isbnCode2, member, LocalDate.now());
    }
    
    @Test
    public void test10ListBooksMustBeingReturnBeforeBorrowingOthers() throws HasLateBooksException, BookNotFoundException {
    	log.info("Test 10 : list of book that member must return before borrowing others");
    	Member member = resident;
    	Book book1 = this.library.borrowBook(isbnCode, member, LocalDate.now().minusDays(Resident.MAX_DAYS + 15)).get();
       	Set<Book> expectedBorrowedBooks = new HashSet<>();
    	expectedBorrowedBooks.add(book1);
    	List<Book> foundBooks = this.library.getLateBooks(member).orElse(new ArrayList<>());
    	assertEquals(expectedBorrowedBooks, new HashSet<>(foundBooks));
    }
}
